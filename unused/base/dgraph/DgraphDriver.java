// package graph.base.dgraph;

// import java.util.Map;

// import javax.annotation.Resource;

// import com.google.protobuf.ByteString;

// import io.dgraph.DgraphClient;
// import io.dgraph.DgraphGrpc;
// import io.dgraph.DgraphGrpc.DgraphStub;
// import io.dgraph.DgraphProto.Mutation;
// import io.dgraph.DgraphProto.Response;
// import io.dgraph.Transaction;
// import io.grpc.ManagedChannel;
// import io.grpc.ManagedChannelBuilder;
// import graph.base.context.ContextService;

// public class DgraphDriver {
//     @Resource()
//     ContextService contextService;

//     private static DgraphDriver instance = null;

//     private static final String TEST_HOSTNAME = "localhost";
//     private static final int TEST_PORT = 9180;

//     private DgraphClient client;

//     public DgraphDriver() {
//     }

//     public static DgraphDriver Instance() {
//         if (instance == null) {
//             instance = new DgraphDriver();
//         }
//         return instance;
//     }

//     public void connect() {
//         ManagedChannel channel =
//         ManagedChannelBuilder.forAddress(TEST_HOSTNAME, TEST_PORT).usePlaintext().build();
//         DgraphStub stub = DgraphGrpc.newStub(channel);
//         this.client = new DgraphClient(stub);
//     }

//     public void openTransaction(DgraphTransactionTypes type) {
//         Transaction newTransaction = null;
//         if (type == DgraphTransactionTypes.READ) {
//             newTransaction = client.newReadOnlyTransaction();
//         }
//         if (type == DgraphTransactionTypes.READ) {
//             newTransaction = client.newTransaction();
//         }
//         contextService.setDgraphTransaction(newTransaction);
//     }

//     public void commitTransaction() {
//         Transaction activeTransaction = contextService.getDgraphTransaction();
//         activeTransaction.commit();
//         activeTransaction.discard();
//     }

//     public void rollbackTransaction() {
//         Transaction activeTransaction = contextService.getDgraphTransaction();
//         activeTransaction.discard();
//     }

//     public ByteString runQuery(String query, Map<String, String> params) {
//         Transaction activeTransaction = contextService.getDgraphTransaction();
//         Response result = activeTransaction.queryWithVars(query, params);
//         return result.getJson();
//     }

//     public void runMutation(String query) {
//         Transaction activeTransaction = contextService.getDgraphTransaction();
//         Mutation mu =
//           Mutation.newBuilder().setSetJson(ByteString.copyFromUtf8(query)).build();
//         activeTransaction.mutate(mu);
//     }
// }
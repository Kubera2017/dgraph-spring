// package graph.base.context;

// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Scope;
// import org.springframework.context.annotation.ScopedProxyMode;
// import org.springframework.web.context.WebApplicationContext;

// import io.dgraph.Transaction;

// public class ContextService {
//     private Transaction activeTransaction;

//     public void setDgraphTransaction(Transaction transaction) {
//         this.activeTransaction = transaction;
//     }

//     public Transaction getDgraphTransaction() {
//         return this.activeTransaction;
//     }
     
//     @Bean
//     @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
//     public ContextService requestContext() {
//         return new ContextService();
//     }
// }
package graph.config;

import org.springframework.stereotype.Component;

@Component
public final class Config {

    public static final String DGRAPH_HOST = "localhost";
    public static final int DGRAPH_PORT = 9080;

}
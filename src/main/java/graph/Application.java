package graph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// import graph.base.dgraph.DgraphDriver;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        // DgraphDriver.Instance().connect();
        SpringApplication.run(Application.class, args);
    }
}
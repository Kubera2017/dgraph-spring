package graph.models;

import java.util.Date;

public abstract class BaseDataModel {
    public String id;
    public Date createdAt;
}
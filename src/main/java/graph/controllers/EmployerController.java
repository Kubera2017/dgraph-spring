package graph.controllers;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.protobuf.ByteString;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.dgraph.DgraphClient;
import io.dgraph.DgraphGrpc;
import io.dgraph.DgraphGrpc.DgraphStub;
import io.dgraph.DgraphProto.Mutation;
import io.dgraph.DgraphProto.Request;
import io.dgraph.DgraphProto.Response;
import io.dgraph.Transaction;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import graph.config.Config;
import graph.models.DgraphEmployersResponse;
import graph.models.Employer;

import org.json.simple.JSONArray; 
import org.json.simple.JSONObject;

@RestController
public class EmployerController {

    private DgraphClient dgraphClient;

    private EmployerController() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(Config.DGRAPH_HOST, Config.DGRAPH_PORT).usePlaintext().build();
        DgraphStub stub = DgraphGrpc.newStub(channel);
        this.dgraphClient = new DgraphClient(stub);
    }

    @GetMapping("/employers")
    public Employer[] getEmployers() {
        String query = 
        "{\n" +
        "  employers(func: allofterms(type, \"EMPLOYER\") ) {\n" +
        "    type\n" +
        "    id\n" +
        "  }\n" +
        "}\n";
        Map<String, String> params = Collections.emptyMap();

        Transaction newTransaction = dgraphClient.newReadOnlyTransaction();

        Response dbResult = newTransaction.queryWithVars(query, params);

        Gson gson = new Gson();
        DgraphEmployersResponse result = gson.fromJson(dbResult.getJson().toStringUtf8(), DgraphEmployersResponse.class);
        return result.employers;
    }

    @PostMapping("/employers")
    public ResponseEntity newEmployer(@RequestBody Employer newEmployer) {
        Gson gson = new Gson();
        String json = gson.toJson(newEmployer);
        Transaction newTransaction = this.dgraphClient.newTransaction();
        Mutation mu = Mutation.newBuilder().setSetJson(ByteString.copyFromUtf8(json.toString())).build();
        newTransaction.mutate(mu);
        newTransaction.commit();

        return ResponseEntity.ok(HttpStatus.OK);
    }

@PostMapping("/employers_test")
    public String likeFeedItem(String feedItemId, String email) {
        try {
        	feedItemId = "20";
        	email = "a1@a.aa";
            String query =
            		"query q($email: string, $feedItemId: string)\n"
            		+ "{\n" +
            		"	user as var(func: allofterms(label, \"USER\"))\n" + 
            		"	@filter(eq(email, \"a1@a.aa\"))\n" +
            		
					"	item as var(func: allofterms(label, \"FEED_ITEM\"))\n" +
					"	@filter(eq(id, \"3\"))\n" +
            		"}\n";
            
            Map <String, String> vars = new HashMap<String, String>();
            vars.put("email", email);
            vars.put("feedItemId", feedItemId);
            
            String mutation = 
            		"uid(item) <likedBy> uid(user) .";
            
            Mutation mu =
            		  Mutation.newBuilder()
            		  .setSetNquads(ByteString.copyFromUtf8(mutation))
            		  .build();
            
            Request request = Request.newBuilder()
            		  .setQuery(query)
            		  .putAllVars(vars)
            		  .addMutations(mu)
            		  .setCommitNow(true)
            		  .build();
            Transaction txn = dgraphClient.newTransaction();
            txn.doRequest(request);
            
            return "OK";
        } catch (Exception e) {
            return e.getMessage();
        }

    }
}